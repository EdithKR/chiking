var express = require("express")
var app = express()
var route_index = require("./routes/index")
var route_info = require("./routes/info")
var route_login = require("./routes/login")
var route_logout = require("./routes/logout")
var mongoose_server = require("./schemas/index")()
var query = require("./query")
var bodyparser = require("body-parser")
var User = require("./schemas/user")
var passport = require("passport")



var session = require("express-session")
//var passport = require("../auth/passport")(app);
//var passport = require('passport')


var MongoStore = require("connect-mongo")(session)


app.set("view engine","ejs");
app.set("views", "./views");
app.use(express.json())
app.use(express.urlencoded({extended:true}));

app.use(session({
    secret:'sdafsgaeffsdv',
    store:new MongoStore({
        mongooseConnection:mongoose_server.connection,
        autoRemove: "interval",
        autoRemoveInterval:1,
    })
}));

require("./auth/passport")(app)

app.use("/", route_index);
app.use("/info", route_info);
app.use("/login",route_login)
app.use("/logout",route_logout)
// app.get("/login", (req,res)=>{
//     res.render("main")
// })


// app.post('/login', passport.authenticate('local', { successRedirect: '/info',
//                                                     failureRedirect: '/login' }));

app.listen(8000,()=>{
    console.log("서버 시작요")
    query.exec();
})
