var express = require("express");
var router = express.Router();
var auth = require("../auth/authcheck")

router.get("/",auth.islogined, (req, res)=>{
    var user = req.user
    console.log(user[0])
    res.render("info",user[0])
})

module.exports = router;