var express = require("express")
    var router = express.Router();
    var passport = require("passport")

    router.post('/', passport.authenticate('local', { successRedirect: '/info',
                                                    failureRedirect: '/login' }));
    
    router.get("/", (req, res)=>{
        res.render("main")
    })

    module.exports = router
