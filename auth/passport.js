module.exports = (app) => {
    var passport = require("passport");
var LocalStrategy = require('passport-local').Strategy;
var User = require("../schemas/user")

app.use(passport.initialize())
app.use(passport.session())

passport.serializeUser(function(user, done) {
    console.log("serial",user)
    done(null, user[0].id);
});

passport.deserializeUser(function(id, done) {
    console.log("deserial", id)
    User.find({id:id},{id:1, pw:1,age:1,phone:1}, function(err, user) {
        done(err, user);
    });
});

    passport.use(new LocalStrategy({
        usernameField: 'id',
        passwordField: 'pw'
      },
        function(id, password, done) {
            console.log("입력받음 값" ,id,password)
            User.find({id:id}, {id:1, pw:1,age:1,phone:1}, function (err, user) {
                console.log("검색결과 ",user)
            if (err) { return done(err); }
            if (user[0].id!=id) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            if (user[0].pw!=password) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, user);
            });
        }
        ));
}