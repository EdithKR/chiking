module.exports = {
    islogined: (req, res, next)=>{
        if(req.isAuthenticated()){
            next()
        }else{
            res.redirect("/login")
        }
    },
    isNotlogined: (req, res, next)=>{
        if(!req.isAuthenticated()){
            next()
        }else{
            res.redirect("/")
        }
    }
}